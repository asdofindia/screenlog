#!/bin/bash

# This is a screenshot script that runs every n minutes, takes screenshot of the desktop and saves it to a folder

# Author: Akshay S Dinesh (asdofindia@gmail.com)
# License: GPL3
# Website: http://learnlearn.in
# Report Bugs at: https://gitlab.com/asdofindia/screenlog

## Installation
################
# Installing this requires that this script is copied to a folder where nobody will delete it, and be run once.
# Step 1: Copy to a secure place. 
# ------
# Remember, files or folders starting with . are hidden in most file browsers on linux. So one good idea might be to copy it to such a folder, say /home/user/.secret
# Step 2: Executable permission
# -------
# For example if you copy screenlog.sh to /home/kaliyuvamane/.secret/screenlog.sh, you have to make it executable by right clicking on it -> properties -> permissions -> allow running this as a program 
# Or open terminal (ctrl+Alt+T), cd /home/kaliyuvamane/.secret/ && chmod +x screenlog.sh
# Step 3: Run once
# -------
# Double click and run, or run using terminal by cd /home/kaliyuvamane/.secret && ./screenlog.sh


## Customization
################

# By default the script adds itself to cron and runs every 10 minutes. To change this choose the appropriate FREQUENCY variable below


# Lines that start with # are comments.
# Uncomment only the right frequency that you need

FREQUENCY="*/5"  #Every 5 minutes
#FREQUENCY="*/10" #EVery 10 minutes


# By default the script saves the files to ".screenlog" directory in home folder. Change it below
IMAGEDIR=".screenlog"


# By default the script runs gnome-screenshot to take the screenshots. If it is not available it might ask you to install that.

SCREENSHOTPROGRAM="gnome-screenshot"
#SCREENSHOTPROGRAM="scrot"

## Uninstalling
###############

# To uninstall, we need to remove the cron entries. 
# To do it manually, run "crontab -e" in terminal. Delete all the lines that reference 
# To do it automatically (untested), run this script with the "uninstall" argument. Like "./screenlog.sh uninstall"


#########################################
# Code starts here

# Get the time for non-conflicting filenames
TIME=`date`

# Save file in the .screenlog directory of the logged in user
USER=`whoami`
DIR=/home/$USER/$IMAGEDIR

# If the directory doesn't exist create it
if [ ! -d "$DIR" ]; then
  mkdir -p "$DIR"
fi

## Add itself to cron to run automatically

# Get the path to itself
# http://stackoverflow.com/questions/4774054/reliable-way-for-a-bash-script-to-get-the-full-path-to-itself#comment22624083_4774063
SCRIPTPATH=$( cd "$(dirname "$0")" ; pwd -P )

SCRIPTNAME="`basename $0`"
TOTALPATH="$SCRIPTPATH"/"$SCRIPTNAME"

SCREENCMD=`which "$SCREENSHOTPROGRAM"`
PROGRAMERROR=$?
if [ $PROGRAMERROR -eq 1 ]; then
  echo "Install $SCREENSHOTPROGRAM or choose another program to take screenshots"
  exit
fi

if [ "$1" != "uninstall" ]
then

  # Line that adds itself to cron http://stackoverflow.com/a/13355743
  (crontab -l ; echo "$FREQUENCY * * * * $TOTALPATH") | sort - | uniq - | crontab -

  # Take screenshot and save
  $SCREENCMD --display=:0 --file="$DIR/$TIME.png"

else
  # Uninstall from crontab http://stackoverflow.com/a/17275608
  ctmp_content=$SCRIPTNAME
  crontab -l | while read -r; do
    [[ $REPLY = *"$ctmp_content"* ]] && continue
    printf '%s\n' "$REPLY"
  done | crontab -
fi
